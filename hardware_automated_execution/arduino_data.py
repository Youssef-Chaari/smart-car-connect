import serial


def read_from_com_port(port='COM2', baudrate=115200, timeout=1):
    ser = None
    try:
        # Open the serial port
        ser = serial.Serial(port, baudrate, timeout=timeout)

        # Check if the port is open
        if ser.is_open:
            print(f"Connected to {port} at {baudrate} baudrate")

            while True:
                if ser.in_waiting >= 9:
                    start_marker = ser.read()
                    if start_marker == b'\x20':
                        pdu = start_marker + ser.read(8)
                        process_climate_control(pdu)
                    elif start_marker == b'\x21':
                        pdu = start_marker + ser.read(5)
                        process_ambient_light(pdu)
                    elif start_marker == b'\x22':
                        pdu = start_marker + ser.read(8)
                        process_seat_control(pdu)

    except serial.SerialException as e:
        print(f"Error opening or reading from {port}: {e}")
    except KeyboardInterrupt:
        print("Exiting program.")
    finally:
        if ser and ser.is_open:
            ser.close()
            print(f"Closed connection to {port}")


def process_climate_control(pdu):
    auto_program_active = pdu[1]
    maximum_cooling_active = pdu[2]
    cooling_function_active = pdu[3]
    rear_window_defroster_active = pdu[4]
    defrost_windows_active = pdu[5]
    system_power_status = pdu[6]
    temperature_setting = pdu[7] / 10.0
    fan_speed = pdu[8]

    print("[ARDUINO] Received climate control data:")
    print(f"[ARDUINO] Auto Program Active: {auto_program_active}")
    print(f"[ARDUINO] Maximum Cooling Active: {maximum_cooling_active}")
    print(f"[ARDUINO] Cooling Function Active: {cooling_function_active}")
    print(f"[ARDUINO] Rear Window Defroster Active: {rear_window_defroster_active}")
    print(f"[ARDUINO] Defrost Windows Active: {defrost_windows_active}")
    print(f"[ARDUINO] System Power Status: {system_power_status}")
    print(f"[ARDUINO] Temperature Setting: {temperature_setting}")
    print(f"[ARDUINO] Fan Speed: {fan_speed}")

    return fan_speed


def process_ambient_light(pdu):
    ambient_light_status = pdu[1] != 0
    ambient_light_color_red = pdu[2]
    ambient_light_color_green = pdu[3]
    ambient_light_color_blue = pdu[4]
    ambient_light_brightness = pdu[5]

    print("[ARDUINO] Received ambient light data:")
    print(f"[ARDUINO] Ambient Light Status: {ambient_light_status}")
    print(f"[ARDUINO] Color Red: {ambient_light_color_red}")
    print(f"[ARDUINO] Color Green: {ambient_light_color_green}")
    print(f"[ARDUINO] Color Blue: {ambient_light_color_blue}")
    print(f"[ARDUINO] Brightness: {ambient_light_brightness}")


def process_seat_control(pdu):
    forward_backward_adjustment = (pdu[1] << 8) | pdu[2]
    recline_angle_adjustment = (pdu[3] << 8) | pdu[4]
    height_adjustment = (pdu[5] << 8) | pdu[6]
    seat_heating = pdu[7] & 0x01
    seat_ventilation = pdu[7] & 0x02
    occupancy_sensor = pdu[7] & 0x04
    seat_belt_status = pdu[7] & 0x08

    print("[ARDUINO] Received seat control data:")
    print(f"[ARDUINO] Forward/Backward Adjustment: {forward_backward_adjustment} cm")
    print(f"[ARDUINO] Recline Angle Adjustment: {recline_angle_adjustment} degrees")
    print(f"[ARDUINO] Height Adjustment: {height_adjustment} cm")
    print(f"[ARDUINO] Seat Heating: {'On' if seat_heating else 'Off'}")
    print(f"[ARDUINO] Seat Ventilation: {'On' if seat_ventilation else 'Off'}")
    print(f"[ARDUINO] Occupancy Sensor: {'Occupied' if occupancy_sensor else 'Unoccupied'}")
    print(f"[ARDUINO] Seat Belt Status: {'Fastened' if seat_belt_status else 'Unfastened'}")


if __name__ == "__main__":
    read_from_com_port()
